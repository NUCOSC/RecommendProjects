# NUCOSC Recommend Projects

> 此项目由[docsify](https://docsify.js.org/#/)部署

NUCOSC (Open Source Community of North University of China) 是由一群热爱代码、爱好开源项目和正版软件，并积极投身编程的爱好者组成并自行维护的社区，本项目是多年以来对于开源项目的接触、使用与学习所总结而来，项目可能不能实时更新，更多项目的更新请参考[HerbertHe stars](https://github.com/HerbertHe?tab=stars) ，如果你喜欢并且支持我们的努力，欢迎star、issue、PR。

* [社区官网](https://www.nucosc.com)
* [国内快速访问](http://zh.nucosc.com)

> 访问 [快速上手](guide) 快速开始
